const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const {
  User
} = require("../models");
const generateHash = () => require("../passport/generateHash");

const configure = (app) => {
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(async function(id, done) {
    const user = await User.findByPk(id);
    done(null, user);
  });

  const config = {
    usernameField: 'userName',
    passwordField: 'password',
    passReqToCallback: false
  };

  passport.use('local', new LocalStrategy(config,
    async function(userName, password, callback) {
      const users = await User.findAll({
        where: {
          username: userName
        },
      });

      const user = users[0];
      if (user.password === generateHash(password)) {
        callback(null, user);
      } else {
        callback(new Error("Invalid credentials"));
      }
    }));

  app.use(passport.initialize());
  app.use(passport.session());
};

module.exports = configure;