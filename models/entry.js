'use strict';
module.exports = (sequelize, DataTypes) => {
  const entry = sequelize.define('entry', {
    userID: DataTypes.INTEGER,
    entries: DataTypes.STRING
  }, {});
  entry.associate = function(models) {
    // associations can be defined here
  };
  return entry;
};