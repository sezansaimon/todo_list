'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
        firstName: 'John',
        lastName: 'Doe',
        userName: 'jhondoe',
        email: 'example@example.com',
        password:'password',
        phoneNumber: '1234567890',
        dob:'01010001',
        gender: 'undefine',
        address:'undefine',
        createdAt: new Date(),
        updatedAt: new Date()
      }])
  }
}
