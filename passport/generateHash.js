const crypto = require('crypto');
const generatehash = (value) => {
  return crypto.createHash('sha256').update(value).digest('hex');
}

module.exports = {
  generatehash
}