const {
  User
} = require('../models')
const Sequelize = require("sequelize");
const passport = require('passport')
const {
  generatehash
} = require('../passport/generateHash')

const renderHomePage = function(req, res) {
  return res.render("users/homepage.pug")
}

const renderSignUp = function(req, res) {
  return res.render("users/signup.pug");
};

//creating user
const signup = async function(req, res) {
  const body = req.body;
  try {
    const user = await User.create({
      firstName: body.firstName,
      lastName: body.lastName,
      userName: body.userName,
      email: body.email,
      password: generatehash(body.password),
      phoneNumber: body.phoneNumber,
      dob: body.dob,
      gender: body.gender,
      address: body.address
    });
    res.redirect(302, '/login');
  } catch (err) {
    console.log('error', err);
    return res.status(504).send({
      error: err.message
    })
  }
}

// List of registrant
const userDetails = async function(req, res) {
  const users = await User.findAll();
  res.render('users/userDetails.pug', {
    users,
    qs: req.query.message
  });
}

const renderLogin = function(req, res) {
  return res.render("users/login.pug");
};

const login = function(req, res) {
  passport.authenticate('local', function(err, user) {
    res.redirect('/entry');
  })(req, res);
};

const forgotPassword = function(req, res) {
  return res.render("users/forgotpassword.pug")
}
module.exports = {
  renderHomePage,
  renderSignUp,
  signup,
  userDetails,
  renderLogin,
  login,
  forgotPassword
};