const {
  entry,
  User
} = require('../models')

const Sequelize = require('sequelize');
const op = Sequelize.op;

const renderTodo = function(req, res) {
  return res.render('posts/entry.pug');
}

//using crud in app
const createEntry = async function(req, res) {
  const body = req.body;
  try {
    const entry = await entry.create({
      entry: body.entry,
      userID: 1
    });
    res.redirect(302, '/entries');
  } catch (err) {
    console.log('error', err);
  }
}
// List of POST - GET,POST
const listEntries = async function(req, res) {
  const posts = await entry.findAll();
  res.render('posts/list.pug', {
    posts,
    qs: req.query.message
  });
}
const renderEditEntry = async function(req, res) {
  const post = await entry.findByPk(req.params.id);
  res.render('posts/editEntry.pug', {
    post,
    id: req.params.id
  });
}
const updateEntry = async function(req, res) {
  const body = req.body;
  //original way
  console.log('Test', body, req.params.id);
  //Await will show its a
  try {
    const post = await entry.update({ //call create statically
      title: body.title,
      body: body.blogDescription
    }, {
      where: {
        id: req.params.id
      }
    });
    res.redirect(302, `/entries/${req.params.id}`);
  } catch (err) {
    console.log('error', err);
    return res.status(504).send({
      error: err.errors[0].message
    })
  }
}

//Delete Implementation
const deleteEntry = async function(req, res) {
  const post = await entry.findByPk(req.params.id);
  await post.destroy();
  res.redirect('/entries?message=delete is done');
}
module.exports = {
  renderTodo,
  createEntry,
  listEntries,
  renderEditEntry,
  updateEntry,
  deleteEntry
}