//importing all required libraries
const express = require('express');
const passport = require("passport");
const bodyParser = require('body-parser');
const session = require("express-session");
const cookieParser = require("cookie-parser");
//adding controller
const entryController = require('./controllers/entries');
const userController = require('./controllers/users');
const passportConfig = require('./config/passport')
// run express on a defined port
const app = express();
const port = process.env.PORT || 3002;
//using pug for rendering
app.set('view engine', 'pug')

const secret = process.env.SECRET || 'jdkfjkdjfkdjfi29djfdfjdjfkdjfkjkjdfkdkfj';
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(session({
  secret
}));
passportConfig(app);
// using http verbs (get, put ,post,patch,delete)
app.get('/', userController.renderHomePage)
//userDetails
app.get('/users', userController.userDetails)
//signup
app.get('/signup', userController.renderSignUp)
app.post('/signup', userController.signup)
app.get('/entry', entryController.renderTodo)
app.post('/entry', entryController.createEntry)
app.get('/entries', entryController.listEntries)
//updating
app.get('/entries/edit/:id', entryController.renderEditEntry);
app.put('/entries/edit/:id', entryController.updateEntry)
app.post('/poentriesst/edit/:id', entryController.updateEntry)
//deleting
app.get('/post/delete/:id', entryController.deleteEntry);
app.get('/login', userController.renderLogin)
app.post('/login', userController.login)
app.get('/forgotpassword', userController.forgotPassword)
//showing port on terminal
app.listen(port, () => console.log(`todo application listening on port ${port}!`))